# AlgorandMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to **string** |  | [optional] [default to "algorand_meta"]
**SenderReward** | Pointer to **string** |  | [optional] 
**RecipientReward** | Pointer to **string** |  | [optional] 
**Close** | Pointer to **string** |  | [optional] 
**CloseAmount** | Pointer to **string** |  | [optional] 
**CloseReward** | Pointer to **string** |  | [optional] 

## Methods

### NewAlgorandMeta

`func NewAlgorandMeta() *AlgorandMeta`

NewAlgorandMeta instantiates a new AlgorandMeta object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAlgorandMetaWithDefaults

`func NewAlgorandMetaWithDefaults() *AlgorandMeta`

NewAlgorandMetaWithDefaults instantiates a new AlgorandMeta object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *AlgorandMeta) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *AlgorandMeta) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *AlgorandMeta) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *AlgorandMeta) HasType() bool`

HasType returns a boolean if a field has been set.

### GetSenderReward

`func (o *AlgorandMeta) GetSenderReward() string`

GetSenderReward returns the SenderReward field if non-nil, zero value otherwise.

### GetSenderRewardOk

`func (o *AlgorandMeta) GetSenderRewardOk() (*string, bool)`

GetSenderRewardOk returns a tuple with the SenderReward field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSenderReward

`func (o *AlgorandMeta) SetSenderReward(v string)`

SetSenderReward sets SenderReward field to given value.

### HasSenderReward

`func (o *AlgorandMeta) HasSenderReward() bool`

HasSenderReward returns a boolean if a field has been set.

### GetRecipientReward

`func (o *AlgorandMeta) GetRecipientReward() string`

GetRecipientReward returns the RecipientReward field if non-nil, zero value otherwise.

### GetRecipientRewardOk

`func (o *AlgorandMeta) GetRecipientRewardOk() (*string, bool)`

GetRecipientRewardOk returns a tuple with the RecipientReward field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRecipientReward

`func (o *AlgorandMeta) SetRecipientReward(v string)`

SetRecipientReward sets RecipientReward field to given value.

### HasRecipientReward

`func (o *AlgorandMeta) HasRecipientReward() bool`

HasRecipientReward returns a boolean if a field has been set.

### GetClose

`func (o *AlgorandMeta) GetClose() string`

GetClose returns the Close field if non-nil, zero value otherwise.

### GetCloseOk

`func (o *AlgorandMeta) GetCloseOk() (*string, bool)`

GetCloseOk returns a tuple with the Close field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetClose

`func (o *AlgorandMeta) SetClose(v string)`

SetClose sets Close field to given value.

### HasClose

`func (o *AlgorandMeta) HasClose() bool`

HasClose returns a boolean if a field has been set.

### GetCloseAmount

`func (o *AlgorandMeta) GetCloseAmount() string`

GetCloseAmount returns the CloseAmount field if non-nil, zero value otherwise.

### GetCloseAmountOk

`func (o *AlgorandMeta) GetCloseAmountOk() (*string, bool)`

GetCloseAmountOk returns a tuple with the CloseAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCloseAmount

`func (o *AlgorandMeta) SetCloseAmount(v string)`

SetCloseAmount sets CloseAmount field to given value.

### HasCloseAmount

`func (o *AlgorandMeta) HasCloseAmount() bool`

HasCloseAmount returns a boolean if a field has been set.

### GetCloseReward

`func (o *AlgorandMeta) GetCloseReward() string`

GetCloseReward returns the CloseReward field if non-nil, zero value otherwise.

### GetCloseRewardOk

`func (o *AlgorandMeta) GetCloseRewardOk() (*string, bool)`

GetCloseRewardOk returns a tuple with the CloseReward field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCloseReward

`func (o *AlgorandMeta) SetCloseReward(v string)`

SetCloseReward sets CloseReward field to given value.

### HasCloseReward

`func (o *AlgorandMeta) HasCloseReward() bool`

HasCloseReward returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


