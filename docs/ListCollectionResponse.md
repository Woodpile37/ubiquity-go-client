# ListCollectionResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | Pointer to [**[]Collection**](Collection.md) |  | [optional] 
**Meta** | Pointer to [**Meta**](Meta.md) |  | [optional] 

## Methods

### NewListCollectionResponse

`func NewListCollectionResponse() *ListCollectionResponse`

NewListCollectionResponse instantiates a new ListCollectionResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListCollectionResponseWithDefaults

`func NewListCollectionResponseWithDefaults() *ListCollectionResponse`

NewListCollectionResponseWithDefaults instantiates a new ListCollectionResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetData

`func (o *ListCollectionResponse) GetData() []Collection`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *ListCollectionResponse) GetDataOk() (*[]Collection, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *ListCollectionResponse) SetData(v []Collection)`

SetData sets Data field to given value.

### HasData

`func (o *ListCollectionResponse) HasData() bool`

HasData returns a boolean if a field has been set.

### GetMeta

`func (o *ListCollectionResponse) GetMeta() Meta`

GetMeta returns the Meta field if non-nil, zero value otherwise.

### GetMetaOk

`func (o *ListCollectionResponse) GetMetaOk() (*Meta, bool)`

GetMetaOk returns a tuple with the Meta field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMeta

`func (o *ListCollectionResponse) SetMeta(v Meta)`

SetMeta sets Meta field to given value.

### HasMeta

`func (o *ListCollectionResponse) HasMeta() bool`

HasMeta returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


